/*
{
"inputs":{
"bucket":"rooms",
"key_filters":[["string_to_int"], ["between", 4200, 4400]]
},
"query":[
{"map":{
"language":"javascript",
"source":
"function(v) {
	var parsed_data = JSON.parse(v.values[0].data);
	var data = {};
	data[Math.floor(v.key / 100).toString()] = parsed_data.capacity;
	return [data];
}"
}},
{"reduce":{
"language":"javascript",
"source":
"function(v) {
	var totals = {};
	for (var i in v) {
		for(var floor in v[i]) {
			if( totals[floor] ) totals[floor] += v[i][floor];
			else totals[floor] = v[i][floor];
		}
	}
	return [totals];
}"
}}
]
}
*/

function(v) {
	var parsed_data = JSON.parse(v.values[0].data);
	var data = {};
	data[Math.floor(v.key / 100).toString()] = parsed_data.capacity;
	return [data];
}

function(v) {
	var totals = {};
	for (var i in v) {
		for(var floor in v[i]) {
			if( totals[floor] ) totals[floor] += v[i][floor];
			else totals[floor] = v[i][floor];
		}
	}
	return [totals];
}