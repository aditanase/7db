--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.4
-- Dumped by pg_dump version 9.3beta2
-- Started on 2013-10-01 10:36:36 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

DROP DATABASE book;
--
-- TOC entry 2382 (class 1262 OID 16393)
-- Name: book; Type: DATABASE; Schema: -; Owner: tase
--

CREATE DATABASE book WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE book OWNER TO tase;

\connect book

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: tase
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO tase;

--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: tase
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 185 (class 3079 OID 11995)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 185
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 190 (class 3079 OID 16394)
-- Name: cube; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 190
-- Name: EXTENSION cube; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION cube IS 'data type for multidimensional cubes';


--
-- TOC entry 188 (class 3079 OID 16487)
-- Name: dict_xsyn; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dict_xsyn WITH SCHEMA public;


--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 188
-- Name: EXTENSION dict_xsyn; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dict_xsyn IS 'text search dictionary template for extended synonym processing';


--
-- TOC entry 187 (class 3079 OID 16492)
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 187
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


--
-- TOC entry 186 (class 3079 OID 16503)
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 186
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- TOC entry 189 (class 3079 OID 16466)
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 189
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


SET search_path = public, pg_catalog;

--
-- TOC entry 280 (class 1255 OID 16615)
-- Name: add_event(text, timestamp without time zone, timestamp without time zone, text, character varying, character); Type: FUNCTION; Schema: public; Owner: tase
--

CREATE FUNCTION add_event(title text, starts timestamp without time zone, ends timestamp without time zone, venue text, postal character varying, country character) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
did_insert boolean := false;
found_count integer;
the_venue_id integer;
BEGIN
SELECT venue_id INTO the_venue_id
FROM venues v
WHERE v.postal_code=postal AND v.country_code=country AND v.name ILIKE venue
LIMIT 1;
IF the_venue_id IS NULL THEN
INSERT INTO venues (name, postal_code, country_code)
VALUES (venue, postal, country)
RETURNING venue_id INTO the_venue_id;
did_insert := true;
END IF;
-- Note: not an “error”, as in some programming languages
RAISE NOTICE 'Venue found %', the_venue_id;
INSERT INTO events (title, starts, ends, venue_id)
VALUES (title, starts, ends, the_venue_id);
RETURN did_insert;
END;
$$;


ALTER FUNCTION public.add_event(title text, starts timestamp without time zone, ends timestamp without time zone, venue text, postal character varying, country character) OWNER TO tase;

--
-- TOC entry 281 (class 1255 OID 16620)
-- Name: log_event(); Type: FUNCTION; Schema: public; Owner: tase
--

CREATE FUNCTION log_event() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
  INSERT INTO logs (event_id, old_title, old_starts, old_ends)
  VALUES (OLD.event_id, OLD.title, OLD.starts, OLD.ends);
  RAISE NOTICE 'Someone just changed event #%', OLD.event_id;
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.log_event() OWNER TO tase;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 176 (class 1259 OID 16560)
-- Name: cities; Type: TABLE; Schema: public; Owner: tase; Tablespace: 
--

CREATE TABLE cities (
    name text NOT NULL,
    postal_code character varying(9) NOT NULL,
    country_code character(2) NOT NULL,
    CONSTRAINT cities_postal_code_check CHECK (((postal_code)::text <> ''::text))
);


ALTER TABLE public.cities OWNER TO tase;

--
-- TOC entry 175 (class 1259 OID 16550)
-- Name: countries; Type: TABLE; Schema: public; Owner: tase; Tablespace: 
--

CREATE TABLE countries (
    country_code character(2) NOT NULL,
    country_name text
);


ALTER TABLE public.countries OWNER TO tase;

--
-- TOC entry 180 (class 1259 OID 16594)
-- Name: events; Type: TABLE; Schema: public; Owner: tase; Tablespace: 
--

CREATE TABLE events (
    event_id integer NOT NULL,
    title character varying(255),
    starts timestamp without time zone,
    ends timestamp without time zone,
    venue_id integer,
    colors text[]
);


ALTER TABLE public.events OWNER TO tase;

--
-- TOC entry 179 (class 1259 OID 16592)
-- Name: events_event_id_seq; Type: SEQUENCE; Schema: public; Owner: tase
--

CREATE SEQUENCE events_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_event_id_seq OWNER TO tase;

--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 179
-- Name: events_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tase
--

ALTER SEQUENCE events_event_id_seq OWNED BY events.event_id;


--
-- TOC entry 182 (class 1259 OID 16622)
-- Name: holidays; Type: VIEW; Schema: public; Owner: tase
--

CREATE VIEW holidays AS
SELECT events.event_id AS holiday_id, events.title AS name, events.starts AS date, events.colors FROM events WHERE (((events.title)::text ~~ '%Day%'::text) AND (events.venue_id IS NULL));


ALTER TABLE public.holidays OWNER TO tase;

--
-- TOC entry 181 (class 1259 OID 16616)
-- Name: logs; Type: TABLE; Schema: public; Owner: tase; Tablespace: 
--

CREATE TABLE logs (
    event_id integer,
    old_title character varying(255),
    old_starts timestamp without time zone,
    old_ends timestamp without time zone,
    logged_at timestamp without time zone DEFAULT now()
);


ALTER TABLE public.logs OWNER TO tase;

--
-- TOC entry 178 (class 1259 OID 16576)
-- Name: venues; Type: TABLE; Schema: public; Owner: tase; Tablespace: 
--

CREATE TABLE venues (
    venue_id integer NOT NULL,
    name character varying(255),
    street_address text,
    type character(7) DEFAULT 'public'::bpchar,
    postal_code character varying(9),
    country_code character(2),
    active boolean DEFAULT true,
    CONSTRAINT venues_type_check CHECK ((type = ANY (ARRAY['public'::bpchar, 'private'::bpchar])))
);


ALTER TABLE public.venues OWNER TO tase;

--
-- TOC entry 177 (class 1259 OID 16574)
-- Name: venues_venue_id_seq; Type: SEQUENCE; Schema: public; Owner: tase
--

CREATE SEQUENCE venues_venue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.venues_venue_id_seq OWNER TO tase;

--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 177
-- Name: venues_venue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: tase
--

ALTER SEQUENCE venues_venue_id_seq OWNED BY venues.venue_id;


--
-- TOC entry 2244 (class 2604 OID 16597)
-- Name: event_id; Type: DEFAULT; Schema: public; Owner: tase
--

ALTER TABLE ONLY events ALTER COLUMN event_id SET DEFAULT nextval('events_event_id_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 16579)
-- Name: venue_id; Type: DEFAULT; Schema: public; Owner: tase
--

ALTER TABLE ONLY venues ALTER COLUMN venue_id SET DEFAULT nextval('venues_venue_id_seq'::regclass);


--
-- TOC entry 2372 (class 0 OID 16560)
-- Dependencies: 176
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: tase
--

INSERT INTO cities VALUES ('Portland', '97205', 'us');
INSERT INTO cities VALUES ('Mogosoaia', '77135', 'ro');


--
-- TOC entry 2371 (class 0 OID 16550)
-- Dependencies: 175
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: tase
--

INSERT INTO countries VALUES ('us', 'United States');
INSERT INTO countries VALUES ('mx', 'Mexico');
INSERT INTO countries VALUES ('au', 'Australia');
INSERT INTO countries VALUES ('gb', 'United Kingdom');
INSERT INTO countries VALUES ('de', 'Germany');
INSERT INTO countries VALUES ('ro', 'Romania');


--
-- TOC entry 2376 (class 0 OID 16594)
-- Dependencies: 180
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: tase
--

INSERT INTO events VALUES (1, 'LARP Club', '2012-02-15 17:30:00', '2012-02-15 19:30:00', 2, NULL);
INSERT INTO events VALUES (2, 'April Fools Day', '2012-04-01 00:00:00', '2012-04-01 23:59:00', NULL, NULL);
INSERT INTO events VALUES (4, 'Moby', '2012-02-06 21:00:00', '2012-02-06 23:00:00', 1, NULL);
INSERT INTO events VALUES (5, 'Wedding', '2012-02-26 21:00:00', '2012-02-26 23:00:00', 2, NULL);
INSERT INTO events VALUES (6, 'Dinner with Mom', '2012-02-26 18:00:00', '2012-02-26 20:30:00', 3, NULL);
INSERT INTO events VALUES (7, 'Valentine’s Day', '2012-02-14 00:00:00', '2012-02-14 23:59:00', NULL, NULL);
INSERT INTO events VALUES (8, 'House Party', '2012-05-03 23:00:00', '2012-05-04 01:00:00', 4, NULL);
INSERT INTO events VALUES (3, 'Christmas Day', '2012-12-25 00:00:00', '2012-12-25 23:59:00', NULL, '{red,green}');


--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 179
-- Name: events_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tase
--

SELECT pg_catalog.setval('events_event_id_seq', 8, true);


--
-- TOC entry 2377 (class 0 OID 16616)
-- Dependencies: 181
-- Data for Name: logs; Type: TABLE DATA; Schema: public; Owner: tase
--

INSERT INTO logs VALUES (8, 'House Party', '2012-05-03 23:00:00', '2012-05-04 02:00:00', '2013-09-30 21:00:12.602824');
INSERT INTO logs VALUES (3, 'Christmas Day', '2012-12-25 00:00:00', '2012-12-25 23:59:00', '2013-09-30 21:24:21.479614');


--
-- TOC entry 2374 (class 0 OID 16576)
-- Dependencies: 178
-- Data for Name: venues; Type: TABLE DATA; Schema: public; Owner: tase
--

INSERT INTO venues VALUES (1, 'Crystal Ballroom', NULL, 'public ', '97205', 'us', true);
INSERT INTO venues VALUES (2, 'Voodoo Donuts', NULL, 'public ', '97205', 'us', true);
INSERT INTO venues VALUES (3, 'Home', 'Poieni 56', 'private', '77135', 'ro', true);
INSERT INTO venues VALUES (4, 'Run''s House', NULL, 'public ', '97205', 'us', true);


--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 177
-- Name: venues_venue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tase
--

SELECT pg_catalog.setval('venues_venue_id_seq', 4, true);


--
-- TOC entry 2251 (class 2606 OID 16568)
-- Name: cities_pkey; Type: CONSTRAINT; Schema: public; Owner: tase; Tablespace: 
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (country_code, postal_code);


--
-- TOC entry 2247 (class 2606 OID 16559)
-- Name: countries_country_name_key; Type: CONSTRAINT; Schema: public; Owner: tase; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_country_name_key UNIQUE (country_name);


--
-- TOC entry 2249 (class 2606 OID 16557)
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: tase; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (country_code);


--
-- TOC entry 2255 (class 2606 OID 16599)
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: tase; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (event_id);


--
-- TOC entry 2253 (class 2606 OID 16586)
-- Name: venues_pkey; Type: CONSTRAINT; Schema: public; Owner: tase; Tablespace: 
--

ALTER TABLE ONLY venues
    ADD CONSTRAINT venues_pkey PRIMARY KEY (venue_id);


--
-- TOC entry 2256 (class 1259 OID 16606)
-- Name: events_starts; Type: INDEX; Schema: public; Owner: tase; Tablespace: 
--

CREATE INDEX events_starts ON events USING btree (starts);


--
-- TOC entry 2257 (class 1259 OID 16605)
-- Name: events_title; Type: INDEX; Schema: public; Owner: tase; Tablespace: 
--

CREATE INDEX events_title ON events USING hash (title);


--
-- TOC entry 2370 (class 2618 OID 16646)
-- Name: delete_venues; Type: RULE; Schema: public; Owner: tase
--

CREATE RULE delete_venues AS ON DELETE TO venues DO INSTEAD UPDATE venues SET active = false WHERE (venues.venue_id = old.venue_id);


--
-- TOC entry 2369 (class 2618 OID 16629)
-- Name: update_holidays; Type: RULE; Schema: public; Owner: tase
--

CREATE RULE update_holidays AS ON UPDATE TO holidays DO INSTEAD UPDATE events SET title = new.name, starts = new.date, colors = new.colors WHERE ((events.title)::text = (old.name)::text);


--
-- TOC entry 2261 (class 2620 OID 16621)
-- Name: log_events; Type: TRIGGER; Schema: public; Owner: tase
--

CREATE TRIGGER log_events AFTER UPDATE ON events FOR EACH ROW EXECUTE PROCEDURE log_event();


--
-- TOC entry 2258 (class 2606 OID 16569)
-- Name: cities_country_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tase
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_country_code_fkey FOREIGN KEY (country_code) REFERENCES countries(country_code);


--
-- TOC entry 2260 (class 2606 OID 16600)
-- Name: events_venue_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tase
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_venue_id_fkey FOREIGN KEY (venue_id) REFERENCES venues(venue_id);


--
-- TOC entry 2259 (class 2606 OID 16587)
-- Name: venues_country_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tase
--

ALTER TABLE ONLY venues
    ADD CONSTRAINT venues_country_code_fkey FOREIGN KEY (country_code, postal_code) REFERENCES cities(country_code, postal_code) MATCH FULL;


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: tase
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM tase;
GRANT ALL ON SCHEMA public TO tase;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-10-01 10:36:36 EEST

--
-- PostgreSQL database dump complete
--

